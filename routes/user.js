const route = require('express').Router()
const patient_master = require('../db/models').patient_master
const response_master = require('../db/models').response_master
const response_details = require('../db/models').response_details


route.post('/patientmaster', (req, res) => {
    patient_master.create({
        patient_id: req.body.patient_id,
        presc_date: req.body.presc_date
    }).then((user) => {
        res.redirect('/patientmaster')
    })
})
route.post('/responsemaster', (req, res) => {
    response_master.create({
        response_id: req.body.response_id,
        patient_id: req.body.patient_id,
        created_on:req.body.created_on
    }).then((user) => {
        res.redirect('/responsemaster')
    })
})
route.post('/responsedetails', (req, res) => {
   response_details.create({
        response_id: req.body.response_id,
        response_text: req.body.response_text
    }).then((user) => {
        res.redirect('/responsedetails')
    })
})

exports = module.exports = route