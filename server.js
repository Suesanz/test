const express = require('express')
const app = express()
const nodemailer = require('nodemailer')
const hbs = require('hbs')
const path = require('path')
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.set('view engine', 'hbs');
app.get('/', (r, s) => {
    s.render('index')
})

app.get('/patientmaster', (r, s) =>
    s.render('patientmaster')
)
app.get('/responsedetails', (r, s) => {
    s.render('responsedetails')
})
app.get('/responsemaster', (r, s) => {
    s.render('responsemaster')
})
// app.use('/user/', express.static(__dirname + '/views'))

//
app.listen(3232,console.log('server started'))